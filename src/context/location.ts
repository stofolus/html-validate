export interface Location {
	filename: string;
	offset: number;
	line: number;
	column: number;
}
