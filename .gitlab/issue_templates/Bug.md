Provide a broad description of the bug.

## Reduced test-case

Show the smallest possible markup that fails.

## Expected result

Given the provided example, what did you expect would happen?

## Actual result

What happened instead?
